const _ = require('lodash');
const {validationResult, matchedData} = require('express-validator');
const csrf = require('csurf');
const config = require('config');
const Executor = require('ssh2-executor');
const Server = require('../models/Server');
const Ping = require("ping-lite");

module.exports = {
    renderListView: (req, res, next) => {
        Server.listAll()
            .then(servers => {
                for (let index in servers) {
                    let ping = new Ping(servers[index].ipAddress);
                    ping.send((error, ms) => {
                        servers[index].health = (!error && ms) ? "green" : "red";
                        Server.updateServer(servers[index].hostname, servers[index]);
                    })
                }
                return servers
            })
            .then(servers => res.render('servers', {
                name: 'serveurs',
                user: req.user,
                title: 'Liste des serveurs enregistrés',
                subTitle: 'Ajoutez ou modifiez les serveurs à surveiller',
                servers
            }))
            .catch(err => {
                throw err;
            });

    },
    renderDashboardView: (req, res, next) => {
        let statut = {
            warning: 0,
            monitoring: 0,
            healthy: 0,
        };
        Server.listAll()
            .then(servers => {
                for (let index in servers) {
                    let ping = new Ping(servers[index].ipAddress);
                    ping.send((error, ms) => {
                        servers[index].health = (!error && ms) ? "green" : "red";
                        Server.updateServer(servers[index].hostname, servers[index]);
                    });
                    statut.monitoring += 1;
                    if (servers[index].health === "green") {
                        statut.healthy += 1;
                    } else {
                        statut.warning += 1;
                    }
                }
                return servers
            })
            .then(servers => res.render('index', {
                name: 'index',
                user: req.user,
                title: 'Monitording',
                servers,
                statut
            }))
            .catch(err => {
                throw err;
            });

    },

    renderDetailsView: (req, res, next) => {
        let cpu;
        let ram;
        let processes = [];
        let users = [];

        Server.findServer(req.params.hostname)
            .then(server => {
                if (server.health === 'green') {
                    Executor.run({
                        host: server.ipAddress,
                        port: server.port,
                        username: server.monitUser,
                        password: server.monitSecret
                    }, [
                        'ps -A -o pcpu | tail -n+2 | paste -sd+ | bc',
                        'free -m | grep \'Mem:\' | awk \'{print substr($3/$2*100, 0, 5)"%"}\'',
                        'getent passwd',
                        'top -b -n 1 | awk \'NR > 7\' | awk \'{print $1,$2,$9,$10,$12}\'',
                        'sudo grep \'^sudo:.*$\' /etc/group | cut -d: -f4'
                    ], (results, t) => {
                        cpu = results[0] + "%";
                        ram = results[1];
                        users = results[2].split('\n');
                        users = users.map(user => user.split(':'));
                        processes = results[3].split('\n');
                        processes = processes.map(process => process.split(' '));
                        let sudo = results[4];
                        let sudo_array = sudo.split(',');
                        sudo_array[sudo_array.length - 1] = sudo_array[sudo_array.length - 1].slice(0, sudo_array[sudo_array.length - 1].length - 1);
                        for (let i in sudo_array) {
                            for (let y in users) {
                                if (users[y][0] === sudo_array[i]) {
                                    users[y].push("green")
                                }
                            }
                        }
                        return res.render('serversDetails', {
                            name: 'serveurs',
                            user: req.user,
                            server,
                            cpu,
                            ram,
                            users,
                            processes
                        })
                    });
                } else {
                    return res.render('serversDetails', {
                        name: 'serveurs',
                        user: req.user,
                        server,
                        cpu,
                        ram,
                        users,
                        processes
                    })
                }
            })
            .catch(err => {
                throw err;
            });
    },

    renderConsole: (req, res, next) => {
        Server.findServer(req.params.hostname)
            .then(server => res.render('console', {
                name: 'serveurs',
                user: req.user,
                server
            }))
            .catch(err => {
                throw err;
            });
    },

    renderAddUser: (req, res, next) => res.render('serversUserForm', {
        name: 'servers',
        user: req.user,
        hostname: req.params.hostname
    }),

    addUser: (req, res, next) => {
        const errors = validationResult(req);
        const data = matchedData(req);

        if (errors.isEmpty()) {
            Server.findServer(req.params.hostname)
                .then(server => {
                    const username = (!!data.privilegedUser) ? data.privilegedUser : server.monitUser;
                    const password = (!!data.privilegedPassword) ? data.privilegedPassword : server.monitSecret;
                    Executor.run({
                        host: server.ipAddress,
                        port: server.port,
                        username,
                        password
                    }, [
                        `echo "${password}" | sudo -S useradd ${data.name} ${(data.uid) ? '--uid ' + data.uid : ''} ${(data.gid) ? '--gid ' + data.gid : ''} ${(data.home) ? '--home ' + data.home : ''} --create-home --shell ${data.console};echo "${password}" | sudo -S su -c 'echo "${data.password}\n${data.password}" | passwd ${data.name}'`
                    ], (results, t) => {
                        if (!!results[0].match(/success/)) {
                            return res.redirect('/servers/' + req.params.hostname)
                        } else {
                            return res.render('serversUserForm', {
                                name: 'servers',
                                user: req.user,
                                hostname: req.params.hostname
                            })
                        }
                    });
                });
        } else {
            res.render('serversUserForm', {
                name: 'servers',
                user: req.user,
                hostname: req.params.hostname
            });
        }
    },

    deleteUser: (req, res, next) => {
        const errors = validationResult(req);
        const data = matchedData(req);
        Server.findServer(req.params.hostname)
            .then(server => {
                const username = (!!data.privilegedUser) ? data.privilegedUser : server.monitUser;
                const password = (!!data.privilegedPassword) ? data.privilegedPassword : server.monitSecret;
                Executor.run({
                    host: server.ipAddress,
                    port: server.port,
                    username,
                    password
                }, [
                    `echo "${password}" | sudo -S userdel -r ${req.params.username}`,
                ], (results, t) => {
                    if (!!results[0].match(/not in the sudoers file/)) {
                        return res.render('serversUserDelete', {
                            user: req.user,
                            hostname: req.params.hostname,
                            username: req.params.username
                        });
                    }
                    return res.redirect('/servers/' + req.params.hostname)
                });
            });
    },

    renderAddView: (req, res, next) => res.render('serversForm', {
        name: 'servers',
        user: req.user,
        title: 'Ajouter un serveur',
        subTitle: 'Remplissez le formulaire pour ajouter un nouveau server'
    }),

    add: (req, res, next) => {
        const errors = validationResult(req);
        const data = matchedData(req);
        const newServer = data;

        if (!errors.isEmpty()) {
            return res.status(422).render('serversForm', {
                title: 'Ajouter un serveur',
                subTitle: 'Corrigez vos erreurs avant de soumettre à nouveau le formulaire',
                errors: errors.mapped(),
                newServeur
            })
        }

        Server.add(newServer, (err, serv, info) => {
            if (err) throw err;
            if (serv) return res.redirect('/servers');

            if (info) return res.status(500).render('serversForm', {
                title: 'Ajouter un serveur',
                subTitle: 'Il y a eu un problème à l\'enregistrement du serveur',
                errors: info,
                newServeur
            })
        })
    },

    renderUpdateView: (req, res, next) => {
        const errors = validationResult(req);
        const {serverHostname} = matchedData(req);

        if (!errors.isEmpty()) {
            return Server.listAll()
                .then(servers => res.status(422).render('servers', {
                    title: 'Liste des serveurs',
                    subTitle: 'Ajoutez ou modifiez les serveurs',
                    servers
                }))
                .catch(err => {
                    throw err;
                });
        }

        Server.findByLogin(serverHostname)
            .then(server => res.render('serversForm', {
                title: 'Mise à jour du serveur',
                subTitle: 'Modifiez ce serveur',
                server,
                serverHostname
            }))
            .catch(err => {
                throw err;
            });
    },

    update: (req, res, next) => {
        const errors = validationResult(req);
        const data = matchedData(req);
        const updatedServer = {
            hostname: data.hostname,
            ipAddress: data.ipAddress,
            monitUser: data.monitUser,
            monitSecret: data.monitSecret,
            osType: data.osType,
            services: data.services
        };

        if (!errors.isEmpty()) {
            return Server.listAll()
                .then(servers => res.status(422).render('servers', {
                    title: 'Liste des serveurs',
                    subTitle: 'Le serveur que vous essayez de modifier n\'existe pas',
                    errors: errors.mapped(),
                    servers
                }))
                .catch(err => {
                    throw err;
                });
        }

        Server.updateServer(data.serverHostname, updatedServer)
            .then(raw => {
                const resStatus = (raw.ok === 1) ? 200 : 422;
                const subTitle = (raw.ok === 1) ? 'Le serveur a correctement été mis à jour' : 'Il y a eu un problème d\'enregistrement....';

                Server.listAll()
                    .then(servers => {
                        res.status(resStatus).render('servers', {
                            title: 'Liste des serveurs',
                            subTitle,
                            servers
                        });
                    })
                    .catch(err => {
                        throw err;
                    })
            })
            .catch(err => {
                throw err;
            });
    },

    renderDeleteView: (req, res, next) => {
        const errors = validationResult(req);
        const {serverHostname} = matchedData(req);

        if (!errors.isEmpty()) {
            return Server.listAll()
                .then(servers => res.status(422).render('servers', {
                    title: 'Liste des servers',
                    subTitle: 'Le serveur demandé n\'existe pas',
                    errors: errors.mapped(),
                    servers
                }))
                .catch(err => {
                    throw err
                })
        }

        Server.findByHostname(serverHostname)
            .then(server => res.render('serverDelete', {
                title: 'Suppression de ' + serverHostname,
                subTitle: 'Confirmez la suppression de ce serveur',
                server,
                serverHostname
            }))
            .catch(err => {
                throw err;
            });
    },

    deleteServer: (req, res, next) => {
        const errors = validationResult(req);
        const {serverHostname} = matchedData(req);

        if (!errors.isEmpty()) {
            return Server.listAll()
                .then(servers => res.status(422).render('serveurs', {
                    title: 'Liste des serveurs',
                    subTitle: 'Le serveur demandé n\'existe pas',
                    errors: errors.mapped(),
                    servers
                }))
                .catch(err => {
                    throw err;
                });
        }

        Server.delete(serverHostname)
            .then(raw => {
                const resStatus = (raw.ok === 1) ? 200 : 422;
                const subTitle = (raw.ok === 1) ? 'Le compte a correctement été désactivé' : 'Il y a eu un problème d\'enregistrement....';

                Server.listAll()
                    .then(servers => {
                        res.status(resStatus).render('serveurs', {
                            title: 'Liste des serveurs',
                            subTitle,
                            serveurs
                        });
                    })
                    .catch(err => {
                        throw err;
                    })
            })
            .catch(err => {
                throw err;
            });
    },

    killProcess: (req, res, next) => {
        Server.findServer(req.params.hostname)
            .then(server => {
                Executor.run({
                    host: server.ipAddress,
                    port: server.port,
                    username: server.monitUser,
                    password: server.monitSecret
                }, [
                    'echo ' + server.monitSecret + ' | sudo -S kill ' + req.params.pid,
                ], (results, t) => {
                    return res.redirect('/servers/' + req.params.hostname)
                });
            })
    },

    addSudo: (req, res, next) => {
        let user = req.params.user;
        Server.findServer(req.params.hostname)
            .then(server => {
                if (server.health === 'green') {
                    Executor.run({
                        host: server.ipAddress,
                        port: server.port,
                        username: server.monitUser,
                        password: server.monitSecret
                    }, [
                        'sudo grep \'^sudo:.*$\' /etc/group | cut -d: -f4'
                    ], (results, t) => {
                        let sudo = results[0];
                        let sudo_array = sudo.split(',');
                        let find = false;
                        for (let i in sudo_array) {
                            let sudoer = sudo_array[i].slice(0, sudo_array[i].length - 1);
                            if (sudoer === user)
                                find = true
                        }
                        if (find) {
                            Executor.run({
                                host: server.ipAddress,
                                port: server.port,
                                username: server.monitUser,
                                password: server.monitSecret
                            }, [
                                'gpasswd -d ' + user + ' sudo'
                            ], (results, t) => {
                                console.log("remove =>");
                                console.log(results)
                            })
                        } else {
                            Executor.run({
                                host: server.ipAddress,
                                port: server.port,
                                username: server.monitUser,
                                password: server.monitSecret
                            }, [
                                'usermod -aG sudo ' + user
                            ], (results, t) => {
                                console.log("add => ");
                                console.log(results)
                            })
                        }
                        return res.redirect('/servers/' + req.params.hostname)
                    }).catch(err => {
                        throw err;
                    });
                }
            });
    }
};