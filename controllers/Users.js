const _ = require('lodash');
const {validationResult, matchedData} = require('express-validator');
const csrf = require('csurf');
const passport = require('passport');
const config = require('config');
const options = config.get('user');
const md5 = require('js-md5');
const User = require('../models/User');

const csrfProtection = csrf({cookie: true});

module.exports = {
    accessIfUnAuthenticated: (req, res, next) => {
        if (!req.isAuthenticated()) {
            next();
        } else {
            res.status(401);
            res.redirect('/');
        }
    },

    accessIfAuthenticated: (req, res, next) => {
        if (req.isAuthenticated()) {
            next();
        } else {
            res.status(401);
            res.redirect('/login');
        }
    },

    accessIfAdmin: (req, res, next) => {
        if (req.isAuthenticated() && req.user.admin) {
            next();
        } else {
            res.status(403).render('index', {name: 'index', title: 'Monitording', error: options.errors.notAdmin})
        }
    },

    register: (req, res, next) => {
        console.log(req.body);
        passport.authenticate('local-register', {badRequestMessage: "Your username and/or your password is missing"}, (err, user, info) => {
            if (err) return next(err);
            if (!user) return res.status(409).render('register', {title: "Inscrivez-vous", subTitle: info.message});

            req.logIn(user, (err) => {
                if (err) return next(err);

                return res.redirect('/');
            });
        })(req, res, next);
    },

    login: (req, res, next) => {
        passport.authenticate('local-login', {badRequestMessage: "Your login and/or your password is missing"}, (err, user, info) => {
            if (err) return next(err);
            if (!user) return res.status(401).render('login', {title: "Connectez-vous", subTitle: info.message});

            req.logIn(user, (err) => {
                if (err) return next(err);

                return res.redirect('/');
            });
        })(req, res, next);
    },

    logout: (req, res, next) => {
        req.logout();
        res.redirect('/login');
    },

    renderListView: (req, res, next) => {
        User.listAll()
            .then(accounts => res.render('accounts/list', {
                title: 'Liste des comptes d\'utilisateurs',
                subTitle: 'Ajoutez ou modifiez les comptes d\'utilisateurs',
                accounts
            }))
            .catch(err => {
                throw err;
            });
    },

    renderAddView: (req, res, next) => res.render('accounts/add', {
        title: 'Ajouter un compte',
        subTitle: 'Remplissez le formulaire pour ajouter un compte d\'utilisateur'
    }),

    add: (req, res, next) => {
        const errors = validationResult(req);
        const data = matchedData(req);
        const newAccount = {
            admin: Boolean(data.admin),
            active: Boolean(data.active),
            lastIp: req.clientIp
        };

        newAccount.local = {
            loginAttempts: 0,
            username: data.username,
            email: data.email,
        };

        console.log(errors.mapped());

        if (!errors.isEmpty()) {
            return res.status(422).render('accounts/add', {
                title: 'Ajouter un compte',
                subTitle: 'Corrigez vos erreurs avant de soumettre à nouveau le formulaire',
                errors: errors.mapped(),
                newAccount
            })
        }

        User.add(newAccount, data.password, (err, user, info) => {
            if (err) throw err;
            if (user) return res.redirect('/account/list');

            if (info) return res.status(500).render('accounts/add', {
                title: 'Ajouter un compte',
                subTitle: 'Il y a eu un problème à l\'enregistrement du compte',
                errors: info,
                newAccount
            })
        })
    },

    renderUpdateView: (req, res, next) => {
        const errors = validationResult(req);
        const {accountUsername} = matchedData(req);

        console.log(accountUsername);
        if (!errors.isEmpty()) {
            return User.listAll()
                .then(accounts => res.status(422).render('accounts/list', {
                    title: 'Liste des comptes d\'utilisateurs',
                    subTitle: 'Ajoutez ou modifiez les comptes d\'utilisateurs',
                    accounts
                }))
                .catch(err => {
                    throw err;
                });
        }

        User.findByLogin(accountUsername)
            .then(user => res.render('accounts/update', {
                title: 'Mise à jour du compte',
                subTitle: 'Modifiez ce compte d\'utilisateur',
                user,
                accountUsername
            }))
            .catch(err => {
                throw err;
            });
    },

    update: (req, res, next) => {
        const errors = validationResult(req);
        const data = matchedData(req);
        const updatedAccount = {
            admin: Boolean(data.admin),
            active: Boolean(data.active),
            'local.username': data.username,
            'local.email': data.email
        };

        if (!errors.isEmpty()) {
            return User.listAll()
                .then(accounts => res.status(422).render('accounts/list', {
                    title: 'Liste des utilisateurs',
                    subTitle: 'Le compte que vous essayez de modifier n\'existe pas',
                    errors: errors.mapped(),
                    accounts
                }))
                .catch(err => {
                    throw err;
                });
        }

        User.updateAccount(data.accountUsername, updatedAccount, data.password)
            .then(raw => {
                const resStatus = (raw.ok === 1) ? 200 : 422;
                const subTitle = (raw.ok === 1) ? 'Le compte a correctement été mis à jour' : 'Il y a eu un problème d\'enregistrement....';

                User.listAll()
                    .then(accounts => {
                        res.status(resStatus).render('accounts/list', {
                            title: 'Liste des utilisateurs',
                            subTitle,
                            accounts
                        });
                    })
                    .catch(err => {
                        throw err;
                    })
            })
            .catch(err => {
                throw err;
            });
    },

    renderDeactivateView: (req, res, next) => {
        const errors = validationResult(req);
        const {accountUsername} = matchedData(req);

        if (!errors.isEmpty()) {
            return User.listAll()
                .then(accounts => res.status(422).render('accounts/list', {
                    title: 'Liste des utilisateurs',
                    subTitle: 'Le compte demandé n\'existe pas',
                    errors: errors.mapped(),
                    accounts
                }))
                .catch(err => {
                    throw err
                })
        }

        User.findByLogin(accountUsername)
            .then(account => res.render('accounts/deactivate', {
                title: 'Désactivation de ' + account.local.username,
                subTitle: 'Confirmez la désactivation de ce compte',
                account,
                accountUsername
            }))
            .catch(err => {
                throw err;
            });
    },

    deactivate: (req, res, next) => {
        const errors = validationResult(req);
        const {accountUsername} = matchedData(req);

        if (!errors.isEmpty()) {
            return User.listAll()
                .then(accounts => res.status(422).render('accounts/list', {
                    title: 'Liste des utilisateurs',
                    subTitle: 'Le compte demandé n\'existe pas',
                    errors: errors.mapped(),
                    accounts
                }))
                .catch(err => {
                    throw err;
                });
        }

        User.deactivate(accountUsername)
            .then(raw => {
                const resStatus = (raw.ok === 1) ? 200 : 422;
                const subTitle = (raw.ok === 1) ? 'Le compte a correctement été désactivé' : 'Il y a eu un problème d\'enregistrement....';

                User.listAll()
                    .then(accounts => {
                        res.status(resStatus).render('accounts/list', {
                            title: 'Liste des utilisateurs',
                            subTitle,
                            accounts
                        });
                    })
                    .catch(err => {
                        throw err;
                    })
            })
            .catch(err => {
                throw err;
            });
    },

    myAccount: (req, res, next) => {
        let user = req.user;
        let email = user.local.email;
        user.hash = md5(email);
        res.render('myaccount', {
            title: 'Compte de ' + user.firstName,
            subTitle: 'Information',
            user
        })
    }
};
