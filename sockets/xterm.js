const SSHClient = require('ssh2').Client;
const utf8 = require('utf8');

const Server = require('../models/Server');

module.exports = io => {
    let server;
    let isConsole;

    io.use((socket, next) => {
        isConsole = !!socket.handshake.query.console;
        Server.findServer(socket.handshake.query.server)
	    .then(result => { 
		server = result;
		return next();
	    })
	    .catch(err => {
		throw err;
	    });
    });

    io.on("connection", function (socket) {
        if (isConsole && !!server && !!server.ipAddress) {
            var ssh = new SSHClient();
            ssh
                .on("ready", function () {
                    socket.emit("data", "\r\n*** SSH CONNECTION ESTABLISHED ***\r\n");
                    connected = true;
                    ssh.shell(function (err, stream) {
                        if (err)
                            return socket.emit(
                                "data",
                                "\r\n*** SSH SHELL ERROR: " + err.message + " ***\r\n"
                            );
                        socket.on("data", function (data) {
                            stream.write(data);
                        });
                        stream
                            .on("data", function (d) {
                                socket.emit("data", utf8.decode(d.toString("binary")));
                            })
                            .on("close", function () {
                                ssh.end();
                            });
                    });
                })
                .on("close", function () {
                    socket.emit("data", "\r\n*** SSH CONNECTION CLOSED ***\r\n");
                })
                .on("error", function (err) {
                    console.log(err);
                    socket.emit(
                        "data",
                        "\r\n*** SSH CONNECTION ERROR: " + err.message + " ***\r\n"
                    );
                })
                .connect({
                    host: server.ipAddress,
                    port: server.port,
                    username: server.monitUser,
                    password: server.monitSecret,
                    //privateKey: server.monitSecret
                });
        }
    });
    setInterval(() => {
        io.on('ready', function () {
            if (!isConsole) {
                Executor.run({
                    host: server.ipAddress,
                    port: server.port,
                    username: server.monitUser,
                    password: server.monitSecret
                }, [
                    'ps -A -o pcpu | tail -n+2 | paste -sd+ | bc',
                    'free -m | grep \'Mem:\' | awk \'{print substr($3/$2*100, 0, 5)"%"}\''

                ], (results) => {
                    return results
                });
            }
        })
    }, 30000)
};
