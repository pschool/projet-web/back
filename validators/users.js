const { check, sanitize  } = require('express-validator');

const User = require('../models/User');

module.exports = {
	login: [
		check('username').trim().isLength({ min: 1 }).withMessage('Vous devez préciser un nom d\'utilisateur'),
		sanitize('username').trim().escape(),

		check('password').trim().isLength({ min: 4 }).withMessage('Le mot de passe doit avoir 4 caractères minimum'),
		sanitize('password').trim().escape(),
	],

	register: [
		check('firstName').trim().isLength({ min: 1 }).withMessage('Vous devez préciser votre prénom'),
		sanitize('firstName').trim().escape(),

		check('lastName').trim().isLength({ min: 1 }).withMessage('Vous devez préciser vitre nom de famille'),
		sanitize('lastName').trim().escape(),
		
		check('username').trim().isLength({ min: 1 }).withMessage('Vous devez préciser un nom d\'utilisateur'),
		sanitize('username').trim().escape(),

		check('email').trim().isLength({ min: 1 }).isEmail().withMessage('Il y a un problème avec cette adresse mail'),
		sanitize('email').trim().escape().normalizeEmail(),

		check('password').trim().isLength({ min: 4 }).withMessage('Le mot de passe doit avoir 4 caractères minimum'),
		sanitize('password').trim().escape(),
	],

	add: [
		check('username').trim().isLength({ min: 1 }).withMessage('Vous devez préciser un nom d\'utilisateur'),
		sanitize('username').trim().escape(),

		check('email').trim().isLength({ min: 1 }).isEmail().withMessage('Il y a un problème avec cette adresse mail'),
		sanitize('email').trim().escape().normalizeEmail(),

		check('password').trim().isLength({ min: 4 }).withMessage('Le mot de passe doit avoir 4 caractères minimum'),
		sanitize('password').trim().escape(),

		check('admin').optional({ checkFalsy: true }).isIn(['admin', 'on']).withMessage('Il y a problème avec l\'option d\'administrateur'),
		sanitize('admin').trim().escape(),

		check('active').optional({ checkFalsy: true }).isIn(['active', 'on']).withMessage('Le compte doit-il être actif ?'),
		sanitize('active').trim().escape()
	],

	/*getUpdate: [
		check('accountUsername').withMessage('Le paramètre doit être un nom d\'utilisateur valide').custom(value => {
			return User.findByLogin(value)
				.then(user => {
					if (!user) throw new Error('Ce compte n\'existe pas');
				})
				.catch(error => {
					throw error;
				});
		})
	],

	update: [
		check('accountUsername').withMessage('Le paramètre doit être un nom d\'utilisateur valide').custom(value => {
			return User.findByLogin(value)
				.then(user => {
					if (!user) throw new Error('Ce compte n\'existe pas');
				})
				.catch(error => {
					throw error;
				});
		}),

		check('username').trim().isLength({ min: 1 }).withMessage('Vous devez préciser un nom d\'utilisateur'),
		sanitize('username').trim().escape(),

		check('email').trim().isLength({ min: 1 }).isEmail().withMessage('Il y a un problème avec cette adresse mail'),
		sanitize('email').trim().escape().normalizeEmail(),

		check('password').optional({ checkFalsy: true }).trim().isLength({ min: 4 }).withMessage('Le mot de passe doit avoir 4 caractères minimum'),
		sanitize('password').trim().escape(),

		check('admin').optional({ checkFalsy: true }).isIn(['admin', 'on']).withMessage('Il y a problème avec l\'option d\'administrateur'),
		sanitize('admin').trim().escape(),

		check('active').optional({ checkFalsy: true }).isIn(['active', 'on']).withMessage('Le compte doit-il être actif ?'),
		sanitize('active').trim().escape()
	],

	deactivate: [
		check('accountUsername').withMessage('Le paramètre doit être un nom d\'utilisateur valide').custom(value => {
			return User.findByLogin(value)
				.then(account => {
					if (!account) throw new Error('Ce compte n\'existe pas');
				})
				.catch(error => {
					throw error;
				});
		})
	]*/
}
