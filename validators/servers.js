const { check, sanitize  } = require('express-validator');

module.exports = {
	add: [
		check('hostname').trim().isLength({ min: 1 }).withMessage('Vous devez préciser un nom d\'hôte'),
		sanitize('hostname').trim().escape(),

		check('ipAddress').trim().isLength({ min: 1 }).withMessage('Il y a un problème avec cette adresse IP'),
		sanitize('ipAddress').trim().escape(),

		check('port').trim().isLength({ min: 1 }).withMessage('Il y a un problème avec ce port'),
		sanitize('port').trim().escape(),

		check('monitUser').trim().isLength({ min: 1 }).withMessage('Vous devez préciser un nom d\'utilisateur'),
		sanitize('monitUser').trim().escape(),

		check('monitSecret').trim().isLength({ min: 1 }).withMessage('Un mot de passe ou une clé privée SSH'),
		sanitize('monitSecret').trim().escape(),

		check('osType').trim().isLength({ min: 1 }).isIn(['windows', 'linux']).withMessage('S\'agit-il d\'un serveur Windows ou Linux'),
		sanitize('osType').trim().escape(),

		check('services').trim().optional(),
		sanitize('services').trim().escape()
	],

	addUser: [
		check('privilegedUser').trim().optional(),
		sanitize('privilegedUser').trim().escape(),

		check('privilegedPassword').trim().optional(),
		sanitize('privilegedPassword').trim(),

		check('name').trim().isLength({ min: 1 }).withMessage('Vous devez préciser un nom d\'hôte'),
		sanitize('name').trim().escape(),

		check('fullName').trim().isLength({ min: 1 }).withMessage('Il y a un problème avec cette adresse IP'),
		sanitize('fullName').trim().escape(),

		check('password').trim().isLength({ min: 1 }).withMessage('Il y a un problème avec cette adresse IP'),
		sanitize('password').trim().escape(),

		check('uid').trim().optional(),
		sanitize('uid').trim().escape(),

		check('gid').trim().optional(),
		sanitize('gid').trim().escape(),

		check('home').trim().optional(),
		sanitize('home').trim(),

		check('console').trim().optional(),
		sanitize('console').trim()
	],

	deleteUser: [
		check('privilegedUser').trim().optional(),
		sanitize('privilegedUser').trim().escape(),

		check('privilegedPassword').trim().optional(),
		sanitize('privilegedPassword').trim(),
	]
}
