const config = require('config');
const crypto = require('crypto');
const semver = require('semver');
const scmp = require('scmp');
const passport = require('passport');

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const options = config.get('user');
const pbkdf2DigestSupport = semver.gte(process.version, '0.12.0');

const User = new Schema({
	local: {
		[options.username.field]: {
			type: String,
			required: true,
			unique: options.username.unique
		},
		[options.password.hashField]: {
			type: String,
			select: false,
			required: true
		},
		[options.password.saltField]: {
			type: String,
			select: false,
			required: true
		},
		email: {
			type: String,
			required: true,
			unique: true
		},
		[options.limitAttempts.attemptsField]: {
			type: Number,
			required: true,
			default: 0
		}
	},
	firstName: {
		type: String,
		required: false
	},
	lastName: {
		type: String,
		required: false
	},
	[options.lastLoginField]: {
		type: Date,
		required: true,
		default: Date.now
	},
	admin: {
		type: Boolean,
		required: true,
		default: false
	},
	servers: [{
		type: Schema.Types.ObjectId,
		ref: 'Server'
	}],
	lastIp: String,
	active: {
		type: Boolean,
		required: true,
		default: true
	}
});

User.pre('save', (next) => {
	if (options.username.lowerCase && options.username.field) {
		this['local.' + options.username.field] = this['local.' + options.username.field].toLowerCase();
	}
	next();
});

User.statics = {
	serializeUser() {
		return (user, callback) => {
			callback(null, user.get('local.' + options.username.field));
		};
	},

	deserializeUser() {
		return (username, callback) => {
			this.findByLogin(username, callback);
		};
	},

	findByLogin(username, opts, callback) {
		if (typeof opts === 'function') {
			callback = opts;
			opts = {};
		}

		if (typeof opts === 'boolean') {
			opts = { selectHashSaltFields: opts  };
		}

		opts = opts || {};

		if (username !== undefined && options.username.lowerCase) {
			username = username.toLowerCase();
		}

		const queryOrParameters = [];
		for (let i = 0; i < options.username.queryFields.length; i++) {
			const parameter = {};
			parameter['local.' + options.username.queryFields[i]] = options.username.caseInsensitive ? new RegExp(`^${username}$`, 'i') : username;
			queryOrParameters.push(parameter);
		}

		const query = this.findOne({ $or: queryOrParameters });

		if (opts.selectHashSaltFields) {
			query.select('+local.' + options.password.hashField + ' +local.' + options.password.saltField);
		}

		if (options.selectFields) query.select(options.selectFields);
		if (options.populateFields) query.populate(options.populateFields);

		if (callback) {
			query.exec(callback);
			return;
		}

		return query;
	},

	password2pbkdf2(password, callback) {
		const promise = Promise.resolve()
			.then(_ => { if (!password) throw new Error(options.errors.missingPassword) })
			.then(_ => new Promise((resolve, reject) => {
				if (password.length < 4) throw new Error(options.errors.passwordTooShort);
				resolve();
			}))
			.then(_ => new Promise((resolve, reject) => {
				crypto.randomBytes(options.password.saltLength, (err, saltBuffer) => (err) ? reject(err) : resolve(saltBuffer))
			}))
			.then((saltBuffer) => saltBuffer.toString(options.password.encoding))
			.then((salt) => {
				if (pbkdf2DigestSupport) {
					return new Promise((resolve, reject) => {
						crypto.pbkdf2(password,
							salt,
							options.password.iterations,
							options.password.keyLength,
							options.password.digestAlgorithm,
							(err, hashRaw) => err ? reject(err) : resolve({
								hash: new Buffer(hashRaw, 'binary').toString(options.password.encoding),
								salt: salt
							}))
					});
				} else {
					return new Promise((resolve, reject) => {
						crypto.pbkdf2(password,
							salt,
							options.password.iterations,
							options.password.keyLength,
							(err, hashRaw) => err ? reject(err) : resolve({
								hash: new Buffer(hashRaw, 'binary').toString(options.password.encoding),
								salt: salt
							}))
					});
				}
			});

		if (!callback) return promise;

		promise
			.then((result) => callback(null, result))
			.catch((err) => callback(err));
	},

	register() {
		return (req, username, password, done) => {
			const account = new this({ lastIp: req.clientIp, firstName: req.body.firstName, lastName: req.body.lastName });
			const response = { user: false, info: {} };
			const promise = Promise.resolve()
				.then(_ => {
					if(!username) {
						response.info.missingUsername = options.errors.missingUsername;
						return;
					}
					account.set('local.' + options.username.field, username);
				})
				.then(_ => {
					if(!req.body.email) {
						response.info.missingEmail = options.errors.missingEmail;
						return;
					}
					account.set('local.email', req.body.email);
				})
				.then(_ => this.findByLogin(account.get('local.' + options.username.field)))
				.then(existingUser => {
					if (existingUser) {
						response.info.userExists = options.errors.userExists
					}
					return;
				})
				.then(_ => this.findByLogin(account.get('local.email')))
				.then(existingEmail => {
					if (existingEmail) {
						response.info.emailExists = options.errors.emailExists;
					}
					return;
				})
				.then(_ => this.password2pbkdf2(password))
				.then(result => {
					account.set('local.' + options.password.hashField, result.hash);
					account.set('local.' + options.password.saltField, result.salt);
				})
				.then(_ => {
					if (Object.keys(response.info).length === 0) {
						return account.save();
					} else {
						return false;
					}
				})
				.then(_ => {
					if (Object.keys(response.info).length === 0) {
						return account.save();
					} else {
						return false;
					}
				}).then(saved => {
					if (saved) {
						response.user = saved;
						response.info = true;
						return response;
					} else {
						response.info.saveError = "Couldn't correctly save your account in our database :(";
						return response;
					}
				});

			if (!done) return promise;

			promise.then(({user, info}) => done(null, user, info))
				.catch(err => done(err));
		}
	},

	authenticate() {
		return (req, login, password, done) => {
			const promise = Promise.resolve()
				.then(_ => this.findByLogin(login, true))
				.then(account => {
					if (account) {
						if (!account.get('local.' + options.password.saltField)) return { user: false, info: { message: options.errors.noSaltValueStored } };
						return account.authenticate(req, password);
					}
					return { user: false, info: { message: options.errors.incorrectUsername } };
				});

			if (!done) return promise;

			promise.then(({ user, info }) => done(null, user, info))
				.catch(err => done(err));
		}
	},
	listAll(includeInactive = false) {
		const query = (includeInactive) ? {} : { active: { $eq: true } };
		return this.find(query).sort('username').exec();
	},

	add(newAccount, password, done) {
		const account = new this(newAccount);
		const response = { user: false, info: {} };
		const promise = Promise.resolve()
			.then(_ => this.findByLogin(account.get('local.' + options.username.field)))
			.then(existingUser => {
				if (existingUser) {
					response.info.userExists = options.errors.userExists
				}
				return;
			})
			.then(_ => this.findByLogin(account.get('local.email')))
			.then(existingEmail => {
				if (existingEmail) {
					response.info.emailExists = options.errors.emailExists;
				}
				return;
			})
			.then(_ => this.password2pbkdf2(password))
			.then(result => {
				account.set('local.' + options.password.hashField, result.hash);
				account.set('local.' + options.password.saltField, result.salt);
			})
			.then(_ => {
				return Challenge.getFirstChallenge();
			})
			.then(firstChallenge => {
				account.set('currentChallenge', firstChallenge._id);
			})
			.then(_ => {
				if (Object.keys(response.info).length === 0) {
					return account.save();
				} else {
					return false;
				}
			}).then(saved => {
				if (saved) {
					response.user = saved;
					response.info = true;
					return response;
				} else {
					response.info.saveError = "Couldn't correctly save your account in our database :(";
					return response;
				}
			});

		if (!done) return promise;

		promise.then(({user, info}) => done(null, user, info))
			.catch(err => done(err));
	},

	updateAccount(accountUsername, updatedInfo, newPassword = null) {
		return Promise.resolve()
			.then(_ => {
				if(newPassword) {
					return this.password2pbkdf2(newPassword);
				} else {
					return false;
				}
			}).then(result => {
				if(result) {
					updatedInfo = {
						'local.hash': result.hash,
						'local.salt': result.salt,
						...updatedInfo
					}
				}
			}).then(_ => {
				return this.update({ 'local.username': accountUsername }, { $set: updatedInfo }).exec();
			})
	},

	deactivate(accountUsername) {
		return this.update({ 'local.username': accountUsername }, { $set: { active: false } }).exec();
	},

	setNextChallenge(login, done) {
		const promise = Promise.resolve()
			.then(_ => {
				return this.findByLogin(login);
			})
			.then(account => {
				return account.setNextChallenge();
			})

		if (!done) return promise;

		promise.then(_ => done(null, true))
			.catch(err => done(err));
	}
}

User.methods = {
	validPassword(submittedPassword, callback) {
		const storedHash = this.get('local.' + options.password.hashField);
		const storedSalt = this.get('local.' + options.password.saltField);
		const promise = Promise.resolve()
			.then(_ => { if (!submittedPassword) throw new options.errors.missingPassword })
			.then(_ => {
				if (pbkdf2DigestSupport) {
					return new Promise((resolve, reject) => {
						crypto.pbkdf2(submittedPassword,
							storedSalt,
							options.password.iterations,
							options.password.keyLength,
							options.password.digestAlgorithm,
							(err, hashBuffer) => err ? reject(err) : resolve(scmp(hashBuffer, new Buffer(storedHash, options.password.encoding))))
					});
				} else {
					return new Promise((resolve, reject) => {
						crypto.pbkdf2(submittedPassword,
							storedSalt,
							options.password.iterations,
							options.password.keyLength,
							(err, hashBuffer) => err ? reject(err) : resolve(scmp(hashBuffer, new Buffer(storedHash, options.password.encoding))))
					});
				}
			});

		if (!callback) return promise;

		promise
			.then(result => callback(null, result))
			.catch(err => callback(err));
	},

	authenticate(req, submittedPassword, callback) {
		console.log(submittedPassword)
		const response = { user: false, info: { message: false } };
		const promise = Promise.resolve()
			.then(_ => {
				this.set('lastIp', req.clientIp);
				if (options.limitAttempts.active) {
					const attemptsInterval = Math.pow(options.limitAttempts.interval, Math.log(this.get('local.' + options.limitAttempts.attemptsField) + 1));
					const calculatedInterval = (attemptsInterval < options.limitAttempts.maxInterval) ? attemptsInterval : options.limitAttempts.maxInterval;

					if (this.get('local.' + options.limitAttempts.attemptsField) >= options.limitAttempts.maxAttempts) {
						response.info.message = options.errors.tooManyAttempts;
						return false;
					}

					if (Date.now() - this.get(options.lastLoginField) < calculatedInterval) {
						response.info.message = options.errors.attemptTooSoon;
						return false;
					}

				}

				this.set('local.' + options.limitAttempts.attemptsField, this.get('local.' + options.limitAttempts.attemptsField) + 1);

				return true;
			})
			.then(isAuthorized => {
				this.set(options.lastLoginField, Date.now());
				if (isAuthorized) {
					return this.validPassword(submittedPassword);
				} else {
					return false;
				}
			})
			.then(isValid => {
				if (!isValid) {
					if (!response.info.message) response.info.message = options.errors.incorrectPassword;
					this.save();
					return false;
				} else {
					this.set('local.' + options.limitAttempts.attemptsField, 0);
					return this.save();
				}
			})
			.then(saved => {
				if (saved) {
					response.user = saved;
					response.info = true;
				}
				return response;
			})

		if (!callback) return promise;

		promise
			.then(result => callback(null, result))
			.catch(err => callback(err))
	},

	setNextChallenge() {
		return Promise.resolve()
			.then(_ => {
				return Challenge.findNextChallenge(this.currentChallenge.rank);
			})
			.then(challenge => {
				this.doneChallenges.push(this.get('currentChallenge'));
				this.set('currentChallenge', challenge._id);
				return this.save();
			})
	}
}

module.exports = mongoose.model('User', User);

