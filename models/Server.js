const mongoose = require('mongoose');
const Schema = mongoose.Schema

const Server = new Schema({
	hostname: {
		type: String,
		required: true,
	},
	ipAddress: {
		type: String,
		required: true,
		unique: true
	},
	port: {
		type: Number,
		required: true,
		default: 22
	},
	monitUser: {
		type: String,
		required: true,
		default: "root"
	},
	monitSecret: {
		type: String,
		required: true
	},
	osType: {
		type: String,
		required: true
	},
	health: {
		type: String,
		required: true,
		default: "red"
	},
	services: [{
		type: String,
		required: true,
		unique: true
	}],
	

});

Server.pre('save', (next) => {
	next();
});

Server.statics = {
	add(serverInfo, done) {
		const newServer = new this(serverInfo);
		newServer.save((err, serv) => {
			if (err) console.log(err);
			return done(err, serv);
		});
	},

	listAll() {
		return this.find({}).sort('hostname').exec();
	},

	findServer(hostname) {
		return this.findOne({ hostname }).exec();
	},

	findServerWithId(_id) {
		return this.findOne({ _id }).exec();
	},

	updateServer(updateAtHostname, updatedServer) {
		return this.updateOne({ hostname: updateAtHostname }, { $set: updatedServer }).exec();
	},

	deleteServer(deleteAtHostname) {
		return this.deleteOne({ hostname: deleteAtHostname }).exec();
	},
}

Server.methods = {
}

module.exports = mongoose.model('Server', Server);

