const express = require('express');
const router = express.Router();
const User = require("../controllers/Users");

/* GET users listing. */
router.get('/', User.accessIfAuthenticated, User.myAccount);

module.exports = router;
