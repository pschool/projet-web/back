const express = require('express');
const passport = require('passport');
const Users = require('../controllers/Users');
const usersValidators = require('../validators/users');
const router = express.Router();
const Servers = require('../controllers/Servers');

router.get('/', Users.accessIfAuthenticated, Servers.renderDashboardView);

router.route('/register')
	.get(Users.accessIfUnAuthenticated, (req, res, next) => { res.render('register', { title: 'Inscription', subTitle: 'Inscrivez-vous et surveillez vos serveurs en toute simplicité !' })})
	.post(Users.accessIfUnAuthenticated, usersValidators.register, Users.register);

router.route('/login')
	.get(Users.accessIfUnAuthenticated, (req, res, next) => { res.render('login', { title: 'Connexion', subTitle: 'Accédez à votre dashboard' })})
	.post(Users.accessIfUnAuthenticated, usersValidators.login, Users.login);

router.get('/logout', Users.accessIfAuthenticated, Users.logout);

module.exports = router;
