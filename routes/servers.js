const express = require('express');
const Users = require('../controllers/Users');
const Servers = require('../controllers/Servers');
const serversValidators = require('../validators/servers');
const router = express.Router();

router.get('/', Users.accessIfAuthenticated, Servers.renderListView);

router.route('/add')
    .get(Users.accessIfAuthenticated, Servers.renderAddView)
    .post(Users.accessIfAuthenticated, serversValidators.add, Servers.add);

router.route('/:hostname')
    .get(Users.accessIfAuthenticated, Servers.renderDetailsView);

router.route('/:hostname/console')
    .get(Users.accessIfAuthenticated, Servers.renderConsole);

router.route('/:hostname/users/add')
    .get(Users.accessIfAuthenticated, Servers.renderAddUser)
    .post(Users.accessIfAuthenticated, serversValidators.addUser, Servers.addUser);

router.route('/:hostname/users/sudo/:user')
    .get(Users.accessIfAuthenticated, Servers.renderDetailsView)
    .post(Users.accessIfAuthenticated, Servers.addSudo);
router.route('/:hostname/users/delete/:username')
    .get(Users.accessIfAuthenticated, Servers.deleteUser)
    .post(Users.accessIfAuthenticated, serversValidators.deleteUser, Servers.deleteUser);

router.route('/:hostname/process/kill/:pid')
    .get(Users.accessIfAuthenticated, Servers.renderDetailsView)
    .post(Users.accessIfAuthenticated, Servers.killProcess);
/*router.route('/update/:id')
	.get(Users.accessIfUnAuthenticated, (req, res, next) => { res.render('serverForm', { title: 'Connexion', subTitle: 'Accédez à votre dashboard' })})
	.post(Users.accessIfUnAuthenticated, serversValidators.update, Servers.update);

router.get('/delete/:id', Users.accessIfAuthenticated, Servers.delete);*/

module.exports = router;
