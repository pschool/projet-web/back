const createError = require('http-errors');
const express = require('express');
const helmet = require('helmet');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const csrf = require('csurf');
const mongoose = require('mongoose');
const config = require('config');
const requestIp = require('request-ip');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);

const indexRouter = require('./routes/index');
const serversRouter = require('./routes/servers');
const usersRouter = require('./routes/users');
const account = require('./routes/account');

const User = require('./models/User');

const dbParams = config.get('database');

const app = express();

app.use(helmet());

mongoose.set('useCreateIndex', true);
mongoose.Promise = global.Promise;
mongoose.connect(`mongodb://${dbParams.username}:${dbParams.password}@${dbParams.host}:${dbParams.port}/${dbParams.name}?${dbParams.options}`, {useNewUrlParser: true})
    .then(_ => console.log('Successfully connected to the database!'))
    .catch(err => {
        console.log(err);
        throw new Error('Could not connect to the database');
    });

app.set('trust proxy', 1);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(requestIp.mw());

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser(config.get('cookieSecret')));
app.use(session({
    name: "monitording_sess",
    cookie: {secure: config.get('https').run},
    secret: config.get('cookieSecret'),
    resave: false,
    saveUninitialized: false,
    store: new MongoStore({
        mongooseConnection: mongoose.connection
    })
}));
app.use(csrf());
app.use(express.static(path.join(__dirname, 'public')));

app.use(passport.initialize());
app.use(passport.session());

passport.use('local-register', new LocalStrategy({passReqToCallback: true}, User.register()));
passport.use('local-login', new LocalStrategy({passReqToCallback: true}, User.authenticate()));

passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

app.use((req, res, next) => {
    res.locals.authenticated = req.isAuthenticated();
    res.locals.admin = (req.user) ? req.user.admin : false;
    res.locals.csrf = req.csrfToken();
    next();
});

app.use('/', indexRouter);
app.use('/servers', serversRouter);
app.use('/users', usersRouter);
app.use('/myaccount', account);
app.use('/tags', function (req, res, next) {
    res.render('wip')
});
app.use('/services', function (req, res, next) {
    res.render('wip')
});

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    res.render('wip');
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;
